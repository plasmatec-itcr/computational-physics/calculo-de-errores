# Cálculo de Errores + Estimación de las superficies transversal y toroidal y el volumen del plasma

Esta carpeta contiene cuadernos de Jupyter en los que se desarrollan los cálculos asociados a la estimación de áreas transversales, toroidales y el volumen de plasma. Además, contiene métodos de estimación del flujo del plasma a través de la superficie transversal encerrada por un camino toroidal seleccionado.
